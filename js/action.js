$('.mmodal__close').on('click', closeModal);
$('.footer__column-button').on('click', showModal);

function showModal() {
    $('.modal-wrapper').css('display', 'flex');
}

function closeModal() {
    $('.modal-wrapper').css('display', 'none');
}

$('.slider__carousel').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    centerMode: true,
    arrows: false
});

$('#arrow_right').click(function () {
    $('.slider__carousel').slick('slickPrev');
});

$('#arrow_left').click(function () {
    $('.slider__carousel').slick('slickNext');
});